<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title;  ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<?php if (isset($og_url)) { ?>
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php } ?>
<?php if (isset($og_image) && $og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<link href="catalog/view/javascript/jquery/jquery-2.1.1.min.js" rel="preload" as="script">
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" ></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="preload" as="style" />
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/bootstrap/js/bootstrap.min.js" rel="preload" as="script">
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" ></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="preload" as="style" />
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="preload" as="style" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />

<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>"  rel="preload" as="style" />
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<link href="catalog/view/theme/rapid/assets/common.js" rel="preload" as="script">
<script src="catalog/view/theme/rapid/assets/common.js" ></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="preload" as="style" />
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<link href="<?php echo $script; ?>" rel="preload" as="script">
<script src="<?php echo $script; ?>" ></script>
<?php } ?>
<link href="catalog/view/theme/rapid/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<header>
  <div class="container">
    <div class="row">
      <div class=" <?php if(!empty($language) || !empty($currency)){?>col-lg-2  <?php } ?>col-lg-3 col-sm-3 col-xs-6">
        <div id="logo">
          <?php if ($logo) { ?>
            <?php if (isset($og_url) && ($home == $og_url)) { ?>
              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
            <?php } else { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
            <?php } ?>
          <?php } else { ?>
          <a href="<?php echo $home; ?>"><?php echo $name; ?></a>
          <?php } ?>
        </div>
      </div>

	  <?php if(!empty($language) || !empty($currency)){?>
		<div class="col-lg-1 col-sm-offset-2 col-lg-offset-0 col-sm-2 col-xs-2 pr-0">
          <div class="btn-group btn-group-head btn-group-justified">
              <div class="btn-group">
                  <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"><svg xmlns="http://www.w3.org/2000/svg" fill="#333" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2 0 .68.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2 0-.68.07-1.35.16-2h4.68c.09.65.16 1.32.16 2 0 .68-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2 0-.68-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z"/></svg></button>
                     <div class="dropdown-menu dropdown-menu-l ">
                         <?php echo $language; ?>
                         <?php echo $currency; ?>
                    </div>
              </div>
          </div>
      </div>
	  <?php } ?>
      <div class="col-lg-2 col-sm-5 col-xs-4 pl-0">
          <div class="btn-group btn-group-head btn-group-justified btn-group-xs" >
                <div class="btn-group" >
                    <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>"  class="btn btn-link dropdown-toggle" data-toggle="dropdown"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 5.9c1.16 0 2.1.94 2.1 2.1s-.94 2.1-2.1 2.1S9.9 9.16 9.9 8s.94-2.1 2.1-2.1m0 9c2.97 0 6.1 1.46 6.1 2.1v1.1H5.9V17c0-.64 3.13-2.1 6.1-2.1M12 4C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 9c-2.67 0-8 1.34-8 4v3h16v-3c0-2.66-5.33-4-8-4z"/><path d="M0 0h24v24H0z" fill="none"/></svg></a>
                       <ul class="dropdown-menu dropdown-menu-l">
                         <?php if ($logged) { ?>
                         <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                         <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                         <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                         <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                         <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                         <?php } else { ?>
                         <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                         <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                         <?php } ?>
                       </ul>
                </div>
                <div class="btn-group" >
                     <a class="btn btn-link" href="<?php echo $wishlist; ?>" id="wishlist-total"   title="<?php echo $text_head_wishlist; ?>">
                         <svg viewBox="0 0 24 24" width="24" height="22" xmlns="http://www.w3.org/2000/svg">
									<path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"></path>
						          </svg><span ><?php echo $text_head_wishlist_value; ?></span></a>
                </div>
          </div>
      </div>
      <div class="col-lg-4  col-sm-8 col-xs-12">
        <?php echo $search; ?>
      </div>
      <div class="col-lg-3 col-sm-4 col-xs-12 ">
          <div class="btn-group  btn-group-justified btn-group-xs" >
                <div class="btn-group" >
                    <?php echo $cart; ?>
                </div>
          </div>
      </div>
    </div>
  </div>

<div class="menu-line">
  <div class="container">
  <div class="row ">
    <div class="col-md-3">
      <?php if ($categories ) { ?>
      <nav id="menu" class="btn-group btn-block">

        <button type="button" class="btn btn-menu btn-block dropdown-toggle"  data-toggle="dropdown" >
          <i class="fa fa-bars"></i>
          <?php echo $text_category; ?>
        </button>
        <ul id="menu-list" class="dropdown-menu">
          <?php foreach ($categories as $category) { ?>
          <?php if ($category['children']) { ?>
          <li >
            <span class="toggle-child">
              <i class="fa fa-plus plus"></i>
              <i class="fa fa-minus minus"></i>
            </span>
            <a class="with-child">
              <i class="fa fa-angle-right arrow"></i>
                <?php echo $category['name']; ?>
              <span class="mobilink hidden-lg hidden-md" onclick="location.href=<?php echo '\'' . $category['href'] . '\''; ?>" ></span>
            </a>
            <?php if ($category['column'] < 2) { ?>
              <?php	$col_class = 'col-md-12'; ?>
              <?php	$box_class = 'box-col-1'; ?>
              <?php	$cols_count = 1; ?>
            <?php } elseif ($category['column'] == 2) { ?>
              <?php	$col_class = 'col-md-6'; ?>
              <?php	$box_class = 'box-col-2'; ?>
              <?php	$cols_count = 2; ?>
            <?php } else { ?>
              <?php	$col_class = 'col-md-4'; ?>
              <?php	$box_class = 'box-col-3'; ?>
              <?php	$cols_count = 3; ?>
            <?php } ?>
            <div class="child-box <?php echo $box_class; ?>">
              <div class="row">
              <?php $i = 0; ?>
              <?php foreach ($category['children'] as $child) { ?>
              <div class="<?php echo $col_class; ?>">
                <div class="child-box-cell">
                  <div class="h5">
                  <?php if($child['children2']) {?>
                  <span class="toggle-child2">
                    <i class="fa fa-plus plus"></i>
                    <i class="fa fa-minus minus"></i>
                  </span>
                  <?php } ?>
                  <a href="<?php echo $child['href']; ?>" class="<?php if($child['children2']) {echo 'with-child2';}?>">
                    <span class="livel-down hidden-md hidden-lg">&#8627;</span><?php echo $child['name']; ?></a></div>
                  <?php if($child['children2'] ) {?>
                  <ul class="child2-box">

                    <?php foreach ($child['children2'] as $child2) { ?>
                    <li><a href="<?php echo $child2['href']; ?>"><span class="livel-down">&#8627;</span><?php echo $child2['name']; ?></a></li>
                  <?php } ?>
                  </ul>
                  <?php } ?>
                </div>
              </div>
                <?php $i++; ?>
                <?php if (($i == $cols_count) &($i != 1)) { ?>
                <div class="clearfix visible-md visible-lg"></div>
                <?php $i = 0; ?>
                <?php } ?>
              <?php } ?>
              </div>
              <div class="see-all-categories hidden-xs hidden-sm">
                <a href="<?php echo $category['href']; ?>"><?php echo $text_all; ?>&nbsp;<?php echo $category['name']; ?></a>
              </div>
            </div>
          </li>
          <?php } else { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <div id="menuMask"></div>
        <script>$('#menu-list').hover(function () {$('body').addClass('blured')},function () {$('body').removeClass('blured')});</script>

      </nav>
      <?php } ?>
    </div>

    <div class="col-md-6 col-xs-10">
      <div id="home-menu">
		<?php if ($header_menu_toggle) {?>
			<?php foreach ($header_menu as $item) { ?>
			<a class="btn" href="<?php echo $item['link'][$language_id]; ?>"><span><?php echo html_entity_decode($item['title'][$language_id], ENT_QUOTES, 'UTF-8'); ?></span></a>
			<?php } ?>
		<?php }?>

		</div>

    </div>
    <div class="col-md-3 col-xs-2">
       <div class="text-right h4 text-phone "><span class="hidden-xs hidden-sm"><?php echo $telephone; ?></span>

         <a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-md hidden-lg">
           <svg class="d-block position-absolute" fill="#ccc" height="22" viewBox="0 0 24 24" width="22" xmlns="http://www.w3.org/2000/svg">
							<path d="M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z"></path>
						</svg>
         </a>
         <ul class="dropdown-menu dropdown-menu-l">
           <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></li>
         </ul>

       </div>




    </div>
  </div>
</div>
</div>
</header>
